<?php
class Router {

    public $URL_NUMPARAM = 0;
    public $URL = array();
    public $QUERY_STRING = array();
    public $QUERY_NUMPARAM = 0;

    public function __construct() {
        # ROUTES - INDEX
        $this->Index();
    }

    private function Index() {
        $URL = $_SERVER['REQUEST_URI'];
        $QUERY_STRING = $_SERVER['QUERY_STRING'];
        # SPLIT URL ? PARAMETERS - QUERY STRING
        if (isset($QUERY_STRING) AND strlen($QUERY_STRING) > 0)
            $URL = (string) str_replace('?' . $QUERY_STRING, '', $URL);
        # PARSE URL
        $URL = explode('/', $URL);  
        $IDX = intval(substr_count(str_replace('http://', '', str_replace('/ ', '', str_replace(' /', '', ' ' . $_SERVER['SERVER_NAME']) . ' ')), '/')) + 1;
        for ($i = 1; $i <= intval(count($URL) - 1); $i++) {
            $this->URL[$i] = (string) isset($URL[$IDX]) ? $URL[$IDX] : '';
            $IDX++;
        }
        $this->URL = array_filter($this->URL);
        reset($this->URL);
        # PARSE QUERY STRING
        parse_str($QUERY_STRING, $this->QUERY_STRING);
        # NUMERO DE PARAMETROS URL
        $this->URL_NUMPARAM = count($this->URL);
        # NUMERO DE PARAMETROS QUERY STRING
        $this->QUERY_NUMPARAM = count($this->QUERY_STRING);
    }

}