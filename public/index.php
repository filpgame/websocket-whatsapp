<?php

if (!defined("DS")) {
    define("DS", DIRECTORY_SEPARATOR);
}
require '../vendor/autoload.php';
require './core/router.class.php';

class Websocket {

    private $template, $router;

    public function __construct() {
        $this->template = new Smarty();
        $this->template->force_compile = false;
        $this->template->debugging = true;
        $this->template->caching = false;
        $this->template->setConfigDir(__DIR__ . DS . ".." . DS . "config" . DS);
        $this->template->setCacheDir(__DIR__ . DS . ".." . DS . "cache" . DS);
        $this->template->setTemplateDir(__DIR__ . DS . ".." . DS . "templates" . DS);
        $this->router = new Router();
        $this->routing();
    }

    public function routing() {
        if ($this->router->URL_NUMPARAM == 2) {
            $this->index();
        }
    }

    public function index() {
        $colours = array('007AFF', 'FF7000', 'FF7000', '15E25F', 'CFC700', 'CFC700', 'CF1100', 'CF00BE', 'F00');
        $this->template->assign("color", $colours[array_rand($colours, 1)]);
        $this->template->assign("titulo", "TESTE");
        $this->template->display("main.tpl");
    }

}

$site = new Websocket();
