<?php

class Servidor {

    const HOST = 'localhost';
    const PORT = '9000';

    private $socket, $clientes, $null = NULL;
    public $debug;

    public function __construct($debug = TRUE) {
        $this->debug = $debug;
        //Create TCP/IP sream socket
        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($this->socket, SOL_SOCKET, SO_REUSEADDR, 1); //reuseable port
        socket_bind($this->socket, 0, self::PORT); //vincula o socket ao host especificado
        socket_listen($this->socket); //Começa a escutar
        //cria e adiciona os sockets dos clientes à lista
        $this->clientes = array($this->socket);
    }

    private function sendMessage(Array $message, $socket = NULL) {
        if ($socket === NULL) {
            $msg = self::encode(json_encode($message));
            foreach ($this->clientes as $changed_socket) {
                @socket_write($changed_socket, $msg, strlen($msg));
            }
            return true;
        } else {
            $msg = self::encode(json_encode($message));
            @socket_write($socket, $msg, strlen($msg));
        }
    }

    //Unmask incoming framed message
    public static function decode($text) {
        $length = ord($text[1]) & 127;
        if ($length == 126) {
            $masks = substr($text, 4, 4);
            $data = substr($text, 8);
        } elseif ($length == 127) {
            $masks = substr($text, 10, 4);
            $data = substr($text, 14);
        } else {
            $masks = substr($text, 2, 4);
            $data = substr($text, 6);
        }
        $text = "";
        for ($i = 0; $i < strlen($data); ++$i) {
            $text .= $data[$i] ^ $masks[$i % 4];
        }
        return $text;
    }

    //Codificar Mensagem para transferir para o Cliente
    public static function encode($text) {
        $b1 = 0x80 | (0x1 & 0x0f);
        $length = strlen($text);
        if ($length <= 125)
            $header = pack('CC', $b1, $length);
        elseif ($length > 125 && $length < 65536)
            $header = pack('CCn', $b1, 126, $length);
        elseif ($length >= 65536)
            $header = pack('CCNN', $b1, 127, $length);
        return $header . $text;
    }

    //handshake new client.
    function clienteHandshake($receved_header, $client_conn) {
        $headers = array();
        $lines = preg_split("/\r\n/", $receved_header);
        foreach ($lines as $line) {
            $line = chop($line);
            if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                $headers[$matches[1]] = $matches[2];
            }
        }
        $secKey = $headers['Sec-WebSocket-Key'];
        $secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
        //hand shaking header
        $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
                "Upgrade: websocket\r\n" .
                "Connection: Upgrade\r\n" .
                "WebSocket-Origin: " . self::HOST . "\r\n" .
                "WebSocket-Location: ws://" . self::HOST . ":" . self::PORT . "/demo/shout.php\r\n" .
                "Sec-WebSocket-Accept:$secAccept\r\n\r\n";
        socket_write($client_conn, $upgrade, strlen($upgrade));
        $response = ['type' => 'id', 'message' => $secAccept];
        $this->sendMessage($response, $client_conn);
        return $secAccept;
    }

    public function startListen() {
        while (true) {
            //manage multipal connections
            $lista_clientes = $this->clientes;
            //returns the socket resources in $changed array
            socket_select($lista_clientes, $this->null, $this->null, 0, 10);
            //check for new socket
            if (in_array($this->socket, $lista_clientes)) {
                $novo_socket = socket_accept($this->socket); //accpet new socket
                $header = socket_read($novo_socket, 1024); //read data sent by the socket
                $cliente_id = $this->clienteHandshake($header, $novo_socket); //perform websocket handshake
                $this->clientes[$cliente_id] = $novo_socket; //add socket to client array

                socket_getpeername($novo_socket, $ip_addr); //captura o ip do novo Cliente
                $response = array('type' => 'system', 'message' => $ip_addr . ' connected'); //prepare json data
                $this->sendMessage($response); //notifica Todos os usuários sobre o novo cliente
                //make room for new socket
                $found_socket = array_search($this->socket, $lista_clientes);
                unset($lista_clientes[$found_socket]);
            }

            //loop através de todas as conexões ligadas
            foreach ($lista_clientes as $cliente) {
                //procura por dados recebidos
                while (socket_recv($cliente, $buf, 1024, 0) >= 1) {
                    $received_text = self::decode($buf); //unmask data
                    $tst_msg = json_decode($received_text); //json decode 
                    if ($tst_msg !== NULL) {
                        $user_name = $tst_msg->name; //sender name
                        $user_message = $tst_msg->message; //message text
                        $user_color = $tst_msg->color; //color
                        $response_text = array('type' => 'usermsg', 'name' => $user_name, 'message' => $user_message, 'color' => $user_color);
                        $this->sendMessage($response_text); //send data
                    }
                    //prepare data to be sent to client
                    break 2; //exist this loop
                }

                $buf = @socket_read($cliente, 1024, PHP_NORMAL_READ);
                if ($buf === false) { // check disconnected client
                    // remove client for $clients array
                    $found_socket = array_search($cliente, $this->clientes);
                    socket_getpeername($cliente, $ip_addr);
                    unset($this->clientes[$found_socket]);

                    //notify all users about disconnected connection
                    $response = ['type' => 'system', 'message' => $ip_addr . ' disconnected'];
                    $this->sendMessage($response);
                }
            }
        }
        // close the listening socket
        socket_close($this->socket);
    }

    public function log($msg) {
        if ($this->debug) {
            $data = date("d-m-Y H:i:s");
            echo "[$data] $msg";
        }
    }

}

$servidor = new Servidor();
$servidor->startListen();
