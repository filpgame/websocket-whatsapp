<?php
require_once '../vendor/autoload.php';
set_time_limit(0);

class FolhaVitoria {

    public $links = [
        "/geral/noticia/todas",
        "/policia/noticia/todas",
        "/politica/noticia/todas",
        "/economia/noticia/todas",
        "/esportes/noticia/todas"
    ];
    public $urlBase = 'http://www.folhavitoria.com.br/';
    public $url = 'http://www.folhavitoria.com.br';
    public $client;

    public function __construct() {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => $this->urlBase
        ]);
    }

    /**
     * 
     * @param string $link
     * @return \DOMXPath
     */
    public function getDOM($link) {
        $response = $this->client->get($link)->getBody()->getContents();
        $DOM = new DOMDocument();
        @$DOM->loadHTML($response);
        $XPath = new DOMXPath($DOM);
        return $XPath;
    }

    public function main() {
        foreach ($this->links as $key => $link) {
            echo "Seção $key ($link)\n";
            for ($i = 1; $i <= 9; $i++) {
                echo "Página $i\n";
                $xp = $this->getDOM($link . "/page%3A$i");
                $nodes = $xp->query("//a[contains(@href,'noticia/2015/05/')]/@href");
                foreach ($nodes as $node) {
                    echo "\t \"<a href='$this->url.$node->textContent'>" . $this->url . $node->textContent . "</a>\",\n";
                }
            }
            echo "\n\n";
        }
    }

}

$folha = new FolhaVitoria();
echo '<pre>';
$folha->main();
