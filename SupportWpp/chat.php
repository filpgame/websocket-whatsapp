<?php

namespace SupportWpp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use SupportWpp\Whatsapp\WhatsListener;

require_once __DIR__ . '/../vendor/autoload.php';

class Chat implements MessageComponentInterface {

    protected $clients;
    public $whatsListener;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->whatsListener = new WhatsListener();
        $eventos = [
            "onGetMessage" => [$this, "onGetMessage"],
            "onConnect" => [$this, "onConnect"],
            "onLoginSuccess" => [$this, "onLoginSuccess"],
        ];
        $this->whatsListener->setListener($eventos);
        $this->whatsListener->iniciaListener();
        $this->poll();
    }

    public function poll() {
        $this->whatsListener->whatsapp->pollMessage();
        $this->whatsListener->whatsapp->sendPing();
    }

    public function onGetMessage($mynumber, $from, $id, $type, $time, $name, $body) {
        \SupportWpp\Log::echoLog("Mensagem $id ($type) recebida no canal $mynumber por $from ($name): '$body'", "green");
        $msg = ["type" => "usermsg", "name" => $name, "message" => $body, "color" => "007AFF"];
        $this->sendToAll($msg, true);
    }

    public function onConnect($mynumber, $socket) {
        \SupportWpp\Log::echoLog("Conta $mynumber Conectada!", 'green');
    }

    public function onLoginSuccess($mynumber, $kind, $status, $creation, $expiration) {
        \SupportWpp\Log::echoLog("$mynumber Logado com sucesso!", 'green');
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->poll();
        \SupportWpp\Log::echoLog("Nova Conexão! ID:({$conn->resourceId}) IP:({$conn->remoteAddress})\n", 'green');
        $msg = ["type" => "system", "message" => "{$conn->remoteAddress} Conectado"];
        $this->sendToAll($msg, true);
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $this->poll();
        if ($msg == '.')
            return;
        $numRecv = count($this->clients) - 1;
        $stringLog = sprintf('Conexao: %d (%s) para %d outras conexoes:' . "\n" . '%s' . "\n", $from->resourceId, $from->remoteAddress, $numRecv, $msg);
        $data = json_decode($msg, true);
        if ($data && (isset($data["name"]) && isset($data["message"]))) {
            $this->whatsListener->whatsapp->sendMessage($data["name"], $data['message']);
        }
        $this->poll();
        \SupportWpp\Log::echoLog($stringLog, 'yellow');
        $this->sendToAll($msg);
    }

    public function onClose(ConnectionInterface $conn) {
        $this->poll();
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        \SupportWpp\Log::echoLog("Connection {$conn->resourceId} has disconnected\n", 'light_red');
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    public function sendToAll($message, $convert_json = FALSE) {
        if ($convert_json) {
            $message = json_encode($message);
        }
        foreach ($this->clients as $client) {
            // The sender is not the receiver, send to each client connected
            $client->send($message);
        }
    }

}

// Run the server application through the WebSocket protocol on port 8080
$app = new \Ratchet\App('localhost', 9000);
$app->route('/chat', new Chat());
$app->route('/echo', new \Ratchet\Server\EchoServer, array('*'));
$app->run();
