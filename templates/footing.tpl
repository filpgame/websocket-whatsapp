<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script>
    var websocket;
    $(document).ready(function () {
        function ping(websock) {
            websock.send(".");
        }
        function initServer() {
            var wsUri = "ws://localhost:9000/chat";
            websocket = new WebSocket(wsUri);

            websocket.onopen = function (ev) { // connection is open 
                $('#message_box').append("<div class=\"system_msg\">Connected!</div>"); //notify user
                var msg = {
                    type: 'usermsg',
                    message: 'OPA!!!',
                    name: '5527996099682',
                    color: '{$color}'
                };
                //convert and send data to server
                websocket.send(JSON.stringify(msg));
                var objDiv = document.getElementById("message_box");
                objDiv.scrollTop = objDiv.scrollHeight;
                setInterval(ping(websocket), 100);
            }
            //#### Message received from server?
            websocket.onmessage = function (ev) {
                var msg = JSON.parse(ev.data); //PHP sends Json data
                var type = msg.type; //message type
                var umsg = msg.message; //message text
                var uname = msg.name; //user name
                var ucolor = msg.color; //color
                console.log(msg);
                if (type == 'usermsg')
                {
                    $('#message_box').append("<div><span class=\"user_name\" style=\"color:#" + ucolor + "\">" + uname + "</span> : <span class=\"user_message\">" + umsg + "</span></div>");
                }
                if (type == 'system')
                {
                    $('#message_box').append("<div class=\"system_msg\">" + umsg + "</div>");
                }
                //$('#message').val(''); //reset text
                var objDiv = document.getElementById("message_box");
                objDiv.scrollTop = objDiv.scrollHeight;
            };

            websocket.onerror = function (ev) {
                $('#message_box').append("<div class=\"system_error\">Error Occurred - " + ev.data + "</div>");
                var objDiv = document.getElementById("message_box");
                objDiv.scrollTop = objDiv.scrollHeight;
            };
            websocket.onclose = function (ev) {
                $('#message_box').append("<div class=\"system_msg\">Connection Closed... Trying to connect again.</div>");
                var objDiv = document.getElementById("message_box");
                objDiv.scrollTop = objDiv.scrollHeight;
                setTimeout(function () {
                    initServer();
                }, 2000);

            };
        }
        //create a new WebSocket object.
        $('#send-btn').click(function () { //use clicks message send button   
            var mymessage = $('#message').val(); //get message text
            var myname = $('#name').val(); //get user name

            if (myname == "") { //empty name?
                alert("Enter your Name please!");
                return;
            }
            if (mymessage == "") { //emtpy message?
                alert("Enter Some message Please!");
                return;
            }

            //prepare json data
            var msg = {
                type: 'usermsg',
                message: mymessage,
                name: myname,
                color: '{$color}'
            };
            //convert and send data to server
            websocket.send(JSON.stringify(msg));
        });
        initServer();
    });
</script>
</html>